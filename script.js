// global variables, blablabla
var slideIndex = 1;
var myTimer = null;
var x = 0;
var xhttp = new XMLHttpRequest();

// do on script-call
document.addEventListener("keyup",function(e)
{
   var key = e.which||e.keyCode;
   switch(key){
      case 37:
         document.getElementById("leftClick").click();
      break;
      case 39:
         document.getElementById("rightClick").click();
      break; 
      case 80:
         autoSlideShow();
      break; 
      case 27:
         closeModal();
      break; 
      case 8:
         closeModal();
      break; 
   }
}); 

// other
function updateText()
{
  xhttp.onreadystatechange = function() 
  {
    if (this.readyState == 4 && this.status == 200) 
    {
      var xmlDoc = this.responseXML;
    
      document.getElementById("imgHeading").innerHTML = xmlDoc.getElementsByTagName("title")[slideIndex - 1].childNodes[0].nodeValue;
      document.getElementById("imgDesc").innerHTML = xmlDoc.getElementsByTagName("desc")[slideIndex - 1].childNodes[0].nodeValue;
    }
  };
  
  xhttp.open("GET", "images.xml", true);
  xhttp.send();  
}

function fillArticleFromXML()
{
  xhttp.onreadystatechange = function() 
  {
    if (this.readyState == 4 && this.status == 200) 
    {
      var xmlDoc = this.responseXML;
      var final_string = "";
      var y = xmlDoc.getElementsByTagName("image");
      var i;
      
      //load static gallery           
      for(i = 0; i < y.length; i++)
      {
        final_string += "<div class=\"column\"><img src=\""+ xmlDoc.getElementsByTagName("src")[i].childNodes[0].nodeValue + "\" style=\"width:100%\" alt=\"IMG" + (i+1) + "_ERR\" onclick=\"openModal();currentSlide(" + (i+1) + ")\" class=\"hover-shadow cursor\"></div>";  
      }                   
      
      //load popup window
      final_string += "<div id=\"myModal\" class=\"modal\"><div id=\"imgHeading\" class=\"imgHeading\">XML_LOAD_FAILED</div><div class=\"modal-content\">";
      
      for(i = 0; i < y.length; i++)
      {
        final_string += "<div class=\"mySlides\"><div class=\"numbertext\">" + (i+1) + " / " + y.length + "</div><img src=\"" + xmlDoc.getElementsByTagName("src")[i].childNodes[0].nodeValue + "\" style=\"width:100%\" alt=\"IMG" + (i+1) + "_ERR\"></div>";  
      } 
      
      final_string += "<span class=\"close cursor unselectable\" onclick=\"closeModal()\">&#10005;</span> <a id=\"leftClick\" class=\"prev unselectable\" onclick=\"checkExistingPresentation(-1)\">&#8630;</a><a id=\"rightClick\" class=\"next unselectable\" onclick=\"checkExistingPresentation(1)\">&#8631;</a><a id=\"slideShow\" class=\"slideShow unselectable\" onclick=\"autoSlideShow()\">&#8635;</a></div><div id=\"imgDesc\" class=\"imgDesc\">XML_LOAD_FAILED</div></div>";
      
      document.getElementById("myArticle").innerHTML = final_string;
    }
  };
  
  xhttp.open("GET", "images.xml", true);
  xhttp.send();
}

function openModal() 
{ 
  updateText();
  document.getElementById('myModal').style.display = "block";
}

function closeModal() 
{
  document.getElementById('myModal').style.display = "none";
  
  if(x == 1)
  {
    clearInterval(myTimer);
    x = 0;
    document.getElementById("slideShow").innerHTML = "&#8635;";
  }  
}

function checkExistingPresentation(n)
{
  if(x == 0)
  {
    plusSlides(n);
  }
}

function plusSlides(n) 
{
  showSlides(slideIndex += n);
}

function currentSlide(n) 
{
  showSlides(slideIndex = n);
}

function showSlides(n) 
{
  var i;
  var slides = document.getElementsByClassName("mySlides");
  
  if (n > slides.length) 
  {
    slideIndex = 1
  }
  
  if (n < 1) 
  {
    slideIndex = slides.length
  }
  
  for (i = 0; i < slides.length; i++) 
  {
    slides[i].style.display = "none";
  }
  
  updateText();
  slides[slideIndex - 1].style.display = "block";  
}

function autoSlideShow()
{
  if(x == 0)
  {
    myTimer = setInterval("plusSlides(1)", 2000);
    x = 1;
    document.getElementById("slideShow").innerHTML = "&#10561;";  
  } 
  else
  {
    clearInterval(myTimer);
    x = 0;
    document.getElementById("slideShow").innerHTML = "&#8635;";
  }
}

